import pytest
import random
from qtmonitor import Monitor


STRUCTURE = ['run', 'Monitor', '__version__']
MONITORS = ['Monitor', 'Custom subclass', 'Custom subclass compact']
GROUPS = ['My Group', 'some group', 'awesomeGroup', '1337t34m', '!"§$%&/()=`0']
VALUES = ['Int', 'Float', 'Bool', 'List', 'Tuple', 'Dict', 'Exception']


class Utils:

    @staticmethod
    def get_layout_children(layout, widget_class=None):
        children = list()
        for i in range(layout.count()):
            item = layout.itemAt(i)
            if item:
                wgt = item.widget()
                if widget_class:
                    if isinstance(wgt, widget_class):
                        children.append(wgt)
                else:
                    children.append(wgt)
        return children


@pytest.fixture
def utils():
    return Utils


@pytest.fixture(params=STRUCTURE)
def structure_part(request):
    yield request.param


@pytest.fixture(params=MONITORS)
def monitor(request, qtbot):
    if request.param == 'Monitor':
        wgt = Monitor()
    else:
        compact = True if request.param.endswith('compact') else False

        class CustomMonitor(Monitor):
            def __init__(self, parent=None):
                super(CustomMonitor, self).__init__(parent, compact)

        wgt = CustomMonitor()

    qtbot.addWidget(wgt)
    return wgt


@pytest.fixture(params=GROUPS)
def group(request):
    yield request.param


@pytest.fixture(params=[1, 2, 5, 10, 25, 50, 100, 250, 500])
def group_num(request):
    yield request.param


@pytest.fixture(params=[1, 2, 5, 10, 25, 50, 100, 250, 500])
def value_num(request):
    yield request.param


@pytest.fixture(params=VALUES)
def value(request):
    val = None
    expected_val = None

    if request.param == 'Int':
        val = random.randrange(0, 100)
        expected_val = str(val)

    if request.param == 'Float':
        val = float(random.randrange(0, 1000) / 1000.0)
        expected_val = str(val)

    if request.param == 'Bool':
        val = random.choice((True, False, None))
        expected_val = str(val)

    if request.param == 'List':
        val = list()
        for _ in range(3):
            val.append(random.randrange(0, 1000))
        expected_val = '<br>'.join([str(x) for x in val])

    if request.param == 'Tuple':
        val = list()
        for _ in range(3):
            val.append(random.randrange(0, 1000))
        val = tuple(val)
        expected_val = '<br>'.join([str(x) for x in val])

    if request.param == 'Dict':
        val = {
            'tx': random.randrange(0, 1000),
            'ty': random.randrange(0, 1000),
            'tz': random.randrange(0, 1000)
        }
        expected_val = '<br>'.join([f'{k}: {v}' for k, v in val.items()])

    if expected_val is None:
        expected_val = val

    test_data = {
        'name': f'{request.param} value',
        'func': lambda: val,
        'expected': expected_val
    }

    if request.param == 'Exception':
        exp_txt = 'Something went wrong'

        def exp_fn():
            raise RuntimeError(exp_txt)

        test_data['func'] = exp_fn
        test_data['expected'] = f'RuntimeError: {exp_txt}'

    yield test_data


