import pytest


@pytest.fixture
def example_monitor(qtbot):
    from qtmonitor.example import ExampleMonitor
    wgt = ExampleMonitor()
    qtbot.addWidget(wgt)
    return wgt


def test_value_fn_int():
    from qtmonitor.example import value_fn_int
    assert isinstance(value_fn_int(), int)


def test_value_fn_float():
    from qtmonitor.example import value_fn_float
    assert isinstance(value_fn_float(), float)


def test_value_fn_str():
    from qtmonitor.example import value_fn_str
    assert isinstance(value_fn_str(), str)


def test_value_fn_bool():
    from qtmonitor.example import value_fn_bool
    for _ in range(100):
        val = value_fn_bool()
        assert val is None or isinstance(val, bool)


def test_value_fn_list():
    from qtmonitor.example import value_fn_list
    assert isinstance(value_fn_list(), list)


def test_value_fn_dict():
    from qtmonitor.example import value_fn_dict
    assert isinstance(value_fn_dict(), dict)


def test_example_monitor(example_monitor, utils):
    from qtmonitor.group import Group
    from qtmonitor.value import Value

    groups = utils.get_layout_children(example_monitor.lay_main, Group)
    assert len(groups) == 2

    for grp in groups:
        values = utils.get_layout_children(grp.lay_content, Value)
        assert len(values) == 6
