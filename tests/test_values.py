def test_add_value(monitor, value):
    grp = monitor.add_group('Some group')
    val = grp.add_value(
        name=value.get('name'),
        func=value.get('func')
    )

    assert val.lbl_value.text() == value.get('expected')


def test_create_and_clear_many_values(monitor, value_num, utils):
    from qtmonitor.group import Value
    grp = monitor.add_group('Test group')

    def some_func():
        return 8

    for i in range(value_num):
        grp.add_value(f'Value {i+1:03d}', some_func, interval=10000)

    values = utils.get_layout_children(grp.lay_content, Value)
    assert len(values) == value_num

    grp.clear()
    assert len(utils.get_layout_children(grp.lay_content, Value)) == 0
