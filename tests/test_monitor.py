def test_object_exists(structure_part):
    import qtmonitor
    assert structure_part in dir(qtmonitor)


def test_monitor_instance(monitor):
    from qtmonitor import Monitor
    assert isinstance(monitor, Monitor)


def test_clean_init(monitor, utils):
    children = utils.get_layout_children(monitor.lay_main)
    if children:
        assert not max(children)





