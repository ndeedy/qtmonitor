def test_add_group(monitor, group, utils):
    grp = monitor.add_group(group)
    assert grp.title() == group
    assert not max(utils.get_layout_children(grp.lay_content))


def test_create_and_clear_many_groups(monitor, group_num, utils):
    from qtmonitor.group import Group

    for i in range(group_num):
        monitor.add_group(f'Group {i:03d}')

    groups = utils.get_layout_children(monitor.lay_main, Group)
    assert len(groups) == group_num

    monitor.clear()
    assert len(utils.get_layout_children(monitor.lay_main, Group)) == 0
