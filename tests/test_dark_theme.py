import pytest
from qtmonitor.base import COLORS_DARK
from qtmonitor.base import set_palette_dark
from PySide2.QtGui import Qt
from PySide2.QtGui import QColor
from PySide2.QtGui import QPalette
from PySide2.QtWidgets import QWidget


THEME = {
    QPalette.Window: 'bg',
    QPalette.WindowText: 'text',
    QPalette.Base: 'base',
    QPalette.AlternateBase: 'bg',
    QPalette.ToolTipBase: 'white',
    QPalette.ToolTipText: 'white',
    QPalette.Text: 'text',
    QPalette.Button: 'bg',
    QPalette.ButtonText: 'text',
    QPalette.BrightText: 'red',
    QPalette.Highlight: 'highlight',
    QPalette.HighlightedText: 'white',
}


@pytest.fixture(params=THEME.keys())
def palette_color(request):
    color_key = THEME.get(request.param)

    if color_key == 'white':
        color = Qt.white

    elif color_key == 'red':
        color = Qt.red

    else:
        color = QColor(*COLORS_DARK.get(color_key))

    yield {'role': request.param, 'color': color}


@pytest.fixture
def widget(qtbot):
    wgt = QWidget()
    set_palette_dark(wgt)
    qtbot.addWidget(wgt)
    return wgt


def test_palette(widget, palette_color):
    plt = widget.palette()
    assert plt.color(palette_color.get('role')) == palette_color.get('color')
