# qtmonitor

Simple tool for creating a Qt-Dialog with updating values. 

The idea behind the project was the necessity to monitor some real-time values 
on a robotic-prototype. The package allows you to create a simple UI with 
a lot of values, that are updating constantly at specified intervals, 
without any knowledge of QT / PySide packages

* [Repository](https://gitlab.com/ndeedy/qtmonitor)
* [PyPI](https://pypi.org/project/qtmonitor)
* [PyPI Test](https://test.pypi.org/project/qtmonitor)


## Setup

To install `qtmonitor` use pip or download it from 
[PyPI](https://pypi.org/project/qtmonitor).

```bash
pip install qtmonitor
```


## Example

There is an example module, which demonstrates how to write your own monitor. 
Feel free to dig down and explore the code. The file is located under 
`you-python-packages-folder/qtmonitor/example.py`

To run and see it in action feel free to use following command:

```bash
python -m qtmonitor.example
```


## Usage

First of all, you need to create some python methods, which would return 
the value, you want to track. Here is a simple random int generator method. 
It will be used to demonstrate the module, there is no practical use for it.

```python
import random

def random_int_value():
    return random.randrange(0, 1000)
```

The easiest way to create you own monitor is to subclass the 
``qtmonitor.Monitor`` class

```python
from qtmonitor import Monitor

class MyMonitor(Monitor):
    def __init__(self, parent=None):
        super(MyMonitor, self).__init__(parent)
```

Now add at least one group to put your values in:

```python
from qtmonitor import Monitor

class MyMonitor(Monitor):
    def __init__(self, parent=None):
        super(MyMonitor, self).__init__(parent)

        grp = self.add_group('My group')

```

Add the values to the group and run the tool. 
Feel free to use provided `run` method:

```python
import random
from qtmonitor import Monitor


def random_int_value():
    return random.randrange(0, 1000)


class MyMonitor(Monitor):
    def __init__(self, parent=None):
        super(MyMonitor, self).__init__(parent)

        grp = self.add_group('My group')
        grp.add_value('Value', random_int_value)
        grp.add_value('Value 10ms', random_int_value, interval=10)

if __name__ == '__main__':
    from qtmonitor import run
    run('My monitor', MyMonitor)
```