from setuptools import setup, find_packages


NAME = 'qtmonitor'
AUTHOR = 'Dietmar Kreider'
EMAIL = 'ndeedy@me.com'
VERSION = '0.2.0'


DESC = 'Simple tool for creating a Qt-Dialog with updating values'
with open('README.md') as readme_file:
    DESC_LONG = readme_file.read()

KEYWORDS = ['Qt', 'PySide2', 'Monitor', 'Debugging', 'Robotics']

with open("requirements.txt") as req_file:
    REQS = req_file.read()

REQS_TEST = ['']

URLS = {
    'Homepage': 'https://gitlab.com/ndeedy/qtmonitor',
    'Repository': 'https://gitlab.com/ndeedy/qtmonitor'
}

CLASSIFIERS = [
    'Development Status :: 4 - Beta',
    'Intended Audience :: Developers',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3.7',
    'License :: OSI Approved :: MIT License',
]

if __name__ == '__main__':
    setup(
        name=NAME,
        version=VERSION,
        license='MIT',
        packages=find_packages(),
        install_requires=REQS,
        tests_require=REQS_TEST,
        author=AUTHOR,
        author_email=EMAIL,
        description=DESC,
        long_description=DESC_LONG,
        long_description_content_type='text/markdown',
        keywords=KEYWORDS,
        url=URLS.get('Repository'),
        project_urls=URLS,
        classifiers=CLASSIFIERS,
        python_requires='>=3.7'
    )
